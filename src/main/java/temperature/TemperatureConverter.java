package temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TemperatureConverter extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private static final String title = "Temperature converter";
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();

    double tempCelsius = Double.parseDouble(request.getParameter("temperatureCelsius"));
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature in celsius: " +
                   tempCelsius + "\n" +
                "  <P>Temperature in fahrenheit: " +
                   celsiusToFahrenheit(tempCelsius) +
                "</BODY></HTML>");
  }

  private double celsiusToFahrenheit(double tempCelsius) {
        return tempCelsius * 1.8 + 32;
  }
}
